#-------------------------------------------------------------------------------
# Base

FROM crystallang/crystal:0.35.1-alpine

SHELL ["/bin/ash", "-c"]

#-------------------------------------------------------------------------------
# Packages

RUN set -exo pipefail              && \
                                      \
    echo 'Install system packages' && \
    apk add --update --no-cache \
      make                      \
      shadow

#-------------------------------------------------------------------------------
# Workspace

RUN set -exo pipefail               && \
                                       \
    echo 'Create working directory' && \
    mkdir -p /work

WORKDIR /work

#-------------------------------------------------------------------------------
# User

ARG HOST_USER_UID=1000
ARG HOST_USER_GID=1000

RUN set -exo pipefail                                  && \
                                                          \
    echo 'Create the notroot user and group from host' && \
    groupadd -g $HOST_USER_GID notroot                 && \
    useradd -lm -u $HOST_USER_UID -g notroot notroot   && \
                                                          \
    echo 'Set direcotry permissions'                   && \
    chown notroot:notroot /work

#-------------------------------------------------------------------------------
# Command

CMD ["sh"]
