#-------------------------------------------------------------------------------
# Docker
#-------------------------------------------------------------------------------

.PHONY: build
build:
	@docker build \
		--build-arg=HOST_USER_UID=`id -u` \
		--build-arg=HOST_USER_GID=`id -g` \
		-t spyglass \
		"$(PWD)"

.PHONY: shell
shell:
	@docker-here -w /work spyglass sh

#-------------------------------------------------------------------------------
# Porject
#-------------------------------------------------------------------------------

.PHONY: shell/compile
shell/compile:
	@docker-here -w /work spyglass make compile

.PHONY: compile
compile:
	@shards build --static

.PHONY: run
run:
	@./bin/spyglass

#-------------------------------------------------------------------------------
# Lint
#-------------------------------------------------------------------------------

.PHONY: lint
lint:
	@docker-here sdwolfz/eclint:latest make lint/eclint
	@docker-here sdwolfz/yamllint:latest make lint/yamllint

.PHONY: lint/eclint
lint/eclint:
	@eclint check $(git ls-files)

.PHONY: lint/yamllint
lint/yamllint:
	@yamllint .
